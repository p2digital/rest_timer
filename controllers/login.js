var helpers = require('../helpers/misc.js');
var db = require("../helpers/db.js");
var bcrypt = require('bcrypt'); // Creates a hash
var crypto = require("../helpers/encrypt.js");


exports.index = function(req, res){


	var name = req.body.name;
	var password = req.body.password;

	//Check the database 
	db.getConnection(function(err, connection){
		connection.query("SELECT * FROM users WHERE username = ?", [name], function(err, result){
			if(!err){
				
				if(result.length!=0){
					connection.release();
					
					//check the hash
					var is_valid_user = bcrypt.compareSync(password, result[0].hash);
					
					//Need to add session management to add session to user and save session
					if(is_valid_user){
						//Set the cookie
						
						var json_string = JSON.stringify(result[0]);
						
						var encrypted_cookie = crypto.encrypt("123456789", json_string)
						
						res.cookie('user_info', encrypted_cookie, {signed: true})
					}

					//Redirect the user
					res.writeHead(302, {
					  'Location': '/recent_timers/'
					  //add other headers here...
					});
					res.end();



				}else{
					console.log("NOTHING FOUND");
				}

				

			}
		});
	});


}