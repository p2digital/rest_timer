

var helpers = require('../helpers/misc.js');
var db = require("../helpers/db.js");
var bcrypt = require('bcrypt');



exports.index = function(req, res){
	
	//

	res.render('register', {title: "Register"});
}

exports.new = function(req, res){

	var name = req.body.name,
	password = req.body.password;

	
	var salt = bcrypt.genSaltSync(10);
	
	var hash = bcrypt.hashSync(password, salt);

	
	db.getConnection(function(err, connection){
			connection.query("INSERT INTO users (username, hash) VALUES (?, ?)", [name, hash], function(err, result){
				if(!err){

					connection.release();

					res.writeHead(302, {
					  'Location': '/register/'
					  //add other headers here...
					});
					res.end();

				}
			});
		});


}
