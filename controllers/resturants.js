

var helpers = require('../helpers/misc.js');
var db = require("../helpers/db.js");



exports.index = function(req, res){
	var rest_id = req.param('number');


	if(!rest_id){
		res.status(404).json({status:"Can't find that resturant"});
	}else{
		//show the info and load the view
		

		

		db.getConnection(function(err, connection){
			connection.query("SELECT * FROM resturant WHERE id = ?", [rest_id], function(err, result){
				if(!err){
					result = result[0];
					res.render('resturant', {result: result})	
				}
				
				connection.release();
			});
		});
		
	}
	//

}


exports.add_new = function(req, res){
	
	res.render('new_rest');
}

exports.add = function(req, res){
	

	var name = req.body.name;
	
	db.getConnection(function(err, connection){
		connection.query("INSERT INTO resturant (name) VALUES (?)", [name], function(err, result){
			if(!err){
				connection.release();
				res.writeHead(302, {
				  'Location': '/resturant/add_new/'
				  //add other headers here...
				});
				res.end();
			}
		})
	})


}