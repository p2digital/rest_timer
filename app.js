
/**
 * Module dependencies.
 */

var express = require('express');


var path = require('path');
var config = require("./config.js");

var app = express();

console.log(config)

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//Add cookie parser
app.use(express.cookieParser(config.cookie_secret));

//Add Session Parser
app.use(express.cookieSession({secret: config.session_secret}));


app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}





var routes = require('./routes')(app);

//var user = require('./routes/user');
//app.get('/', routes.index);
//app.get('/users', user.list);

module.exports = app;
