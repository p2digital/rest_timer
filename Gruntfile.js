module.exports = function(grunt) {
	// Do grunt-related things in here

	// load all grunt tasks
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);



	grunt.initConfig({


		watch: {
			scripts: {
				files: ['app_static_files/styl/*.styl', "app_static_files/js/*.js"],
				tasks: ['stylus', "concat", 'uglify'],
				//tasks: ['stylus', "concat", 'minified'],
				options: {
					//interrupt: true,
				}
			}
		},
		stylus: {
			compile: {
				options: {
					firebug : false,
					linenos : false,
					compress : false
				},
				files: {
					// 'path/to/result.css': 'path/to/source.styl', // 1:1 compile
					'public/css/main.css': ['app_static_files/styl/reset.styl', 'app_static_files/styl/main.styl']//['path/to/sources/*.styl', 'path/to/more/*.styl'] // compile and concat into single file
				}
			}
		},
		concat: {
			dist: {
				src: ['app_static_files/js/*.js'],
				dest: 'public/js/main.js'
			}
		},
		jshint: {
			beforeconcat: ['app_static_files/js/*.js'],
			afterconcat: ['public/js/main.js'],
			ignore_warning: {
				options: {
					'-W033': true,
				},
				src: ['**/*.js'],
			}
		},
		uglify: {
			options: {
				mangle : false,
				beautify : true,
				preserveComments : 'some'
			},
			my_target: {
				files: {
					"public/js/main.js" : "public/js/main.js"
				}
			}
		},
		minified : {
		  files: {
		    src: [
		    '/app_static_files/js/main.js'
		    ],
		    dest: 'public/js/'
		  },
		  options : {
		  	dest_filename: "main.js",
		    sourcemap: true,
		    allinone: false
		  }
		}

	});
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.registerTask('default', 'watch');

};