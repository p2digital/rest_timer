module.exports = function(app){




	var homepage = require('./controllers/index');
	var recent_timers = require("./controllers/recent_timers");
	var resturants = require("./controllers/resturants");
	var register = require("./controllers/register");
	var login = require("./controllers/login");
	var logout = require("./controllers/logout");


	//Homepage
	app.get("/", homepage.index);

	//Recent Timers
	app.get("/recent_timers", recent_timers.index);



	app.get("/resturant/add_new", resturants.add_new);
	app.post("/resturant/add/", resturants.add);
	app.get("/resturant/:number", resturants.index);


	app.post("/register/user/", register.new);
	app.get("/register/", register.index);



	app.post("/login/", login.index);

	app.get('/logout/', logout.index);

	
}