//https://npmjs.org/package/mysql


var mysql = require('mysql');
var pool  = mysql.createPool({
	host:"localhost",
	user: "root",
	password: "root",
	database: "rest_timer",
	port: 8889
});

exports.getConnection = function(callback) {
    pool.getConnection(function(err, connection) {
        callback(err, connection);
    });
};
