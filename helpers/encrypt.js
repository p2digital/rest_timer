

var crypto = require("crypto");
var algorithm = "aes256";


//Encrypt (Encryption key(this should be in the config file), (string (stringified json object) ) )
exports.encrypt = function(key, text) {
        var cipher = crypto.createCipher(algorithm, key);
        return cipher.update(text, "utf8", "hex") + cipher.final("hex");
};

exports.decrypt = function(key, encrypted) {
        var decipher = crypto.createDecipher(algorithm, key);
        return decipher.update(encrypted, "hex", "utf8") + decipher.final("utf8");
};

